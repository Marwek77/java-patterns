package sk.mwk77.tutorialspoint.abstractfactorypattern;

public class Rectangle implements Shape {

  @Override
  public void draw() {
    System.out.println("Inside Rectangle::draw() method.");
  }
}
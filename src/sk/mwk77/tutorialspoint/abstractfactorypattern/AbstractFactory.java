package sk.mwk77.tutorialspoint.abstractfactorypattern;

abstract class AbstractFactory {

  abstract Shape getShape(String shapeType) ;
}
package sk.mwk77.tutorialspoint.abstractfactorypattern;

public interface Shape {

  void draw();
}
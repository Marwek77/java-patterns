package sk.mwk77.tutorialspoint.factorypattern;

public interface Shape {

  void draw();
}
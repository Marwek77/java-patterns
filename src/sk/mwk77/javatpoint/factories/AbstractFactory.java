package sk.mwk77.javatpoint.factories;

import sk.mwk77.javatpoint.bank.Bank;
import sk.mwk77.javatpoint.loan.Loan;

public abstract class AbstractFactory{

  public abstract Bank getBank(String bank);
  public abstract Loan getLoan(String loan);
}
package sk.mwk77.javatpoint.factories;

import sk.mwk77.javatpoint.bank.Bank;
import sk.mwk77.javatpoint.bank.HDFC;
import sk.mwk77.javatpoint.bank.ICICI;
import sk.mwk77.javatpoint.bank.SBI;
import sk.mwk77.javatpoint.loan.Loan;

class BankFactory extends AbstractFactory{

  public Bank getBank(String bank){
    if(bank == null){
      return null;
    }
    if(bank.equalsIgnoreCase("HDFC")){
      return new HDFC();
    } else if(bank.equalsIgnoreCase("ICICI")){
      return new ICICI();
    } else if(bank.equalsIgnoreCase("SBI")){
      return new SBI();
    }
    return null;
  }
  public Loan getLoan(String loan) {
    return null;
  }
}
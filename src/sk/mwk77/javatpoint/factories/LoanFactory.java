package sk.mwk77.javatpoint.factories;

import sk.mwk77.javatpoint.bank.Bank;
import sk.mwk77.javatpoint.loan.Loan;
import sk.mwk77.javatpoint.loan.HomeLoan;
import sk.mwk77.javatpoint.loan.EducationLoan;
import sk.mwk77.javatpoint.loan.BusinessLoan;

class LoanFactory extends AbstractFactory{

  public Bank getBank(String bank){
    return null;
  }

  public Loan getLoan(String loan){
    if(loan == null){
      return null;
    }
    if(loan.equalsIgnoreCase("Home")){
      return new HomeLoan();
    } else if(loan.equalsIgnoreCase("Business")){
      return new BusinessLoan();
    } else if(loan.equalsIgnoreCase("Education")){
      return new EducationLoan();
    }
    return null;
  }
}
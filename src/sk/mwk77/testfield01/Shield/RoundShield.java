package sk.mwk77.testfield01.Shield;

public class RoundShield extends Shields {

  static RoundShield valueOf(int health, int shdAtt, int shdDef, String name) {
    return new RoundShield(health, shdAtt, shdDef, name);
  }

  private RoundShield(int health, int shdAtt, int shdDef, String name) {
    this.health = health;
    this.shdAtt = shdAtt;
    this.shdDef = shdDef;
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public int getShdAtt() {
    return shdAtt;
  }

  public int getShdDef() {
    return shdDef;
  }

  public int getHealth() {
    return health;
  }

  public void setHealth(int health) {
    this.health = health;
  }

  @Override
  public String toString() {
    System.out.println(String.format("RoundShield name: %s", name));
    return super.toString();
  }
}
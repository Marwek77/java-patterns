package sk.mwk77.testfield01.Shield;

public abstract class Shields {

  String name;
  int shdAtt;
  int shdDef;

  int health;

  public abstract String getName();

  public abstract int getShdAtt();

  public abstract int getShdDef();

  public abstract int getHealth();

  public abstract void setHealth(int health);

  @Override
  public String toString() {
    return String.format("is %d %d %d %s", getHealth(), getShdAtt(), getShdDef(), getName());
  }
}
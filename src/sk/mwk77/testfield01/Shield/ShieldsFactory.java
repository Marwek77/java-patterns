package sk.mwk77.testfield01.Shield;

public class ShieldsFactory {

  public static Shields createShield(int getHealth, int getShdAtt, int getShdDef, String name, ShieldsType type) {

    Shields weapons = null;

    switch (type) {
      case BigShield:
        weapons = BigShield.valueOf(getHealth, getShdAtt, getShdDef, name);
        break;
      case SmallShield:
        weapons = SmallShield.valueOf(getHealth, getShdAtt, getShdDef, name);
        break;
      case RoundShield:
        weapons = RoundShield.valueOf(getHealth, getShdAtt, getShdDef, name);
        break;
    }
    return weapons;
  }
}
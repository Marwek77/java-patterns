package sk.mwk77.testfield01.Shield;

public class BigShield extends Shields {

  static BigShield valueOf(int health, int shdAtt, int shdDef, String name) {
    return new BigShield(health, shdAtt, shdDef, name);
  }

  private BigShield(int health, int shdAtt, int shdDef, String name) {
    this.health = health;
    this.shdAtt = shdAtt;
    this.shdDef = shdDef;
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public int getShdAtt() {
    return shdAtt;
  }

  public int getShdDef() {
    return shdDef;
  }

  public int getHealth() {
    return health;
  }

  public void setHealth(int health) {
    this.health = health;
  }

  @Override
  public String toString() {
    System.out.println(String.format("BigShield name: %s", name));
    return super.toString();
  }
}
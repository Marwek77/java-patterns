package sk.mwk77.testfield01.Shield;

public class SmallShield extends Shields {

  static SmallShield valueOf(int health, int shdAtt, int shdDef, String name) {
    return new SmallShield(health, shdAtt, shdDef, name);
  }

  private SmallShield(int health, int shdAtt, int shdDef, String name) {
    this.health = health;
    this.shdAtt = shdAtt;
    this.shdDef = shdDef;
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public int getShdAtt() {
    return shdAtt;
  }

  public int getShdDef() {
    return shdDef;
  }

  public int getHealth() {
    return health;
  }

  public void setHealth(int health) {
    this.health = health;
  }

  @Override
  public String toString() {
    System.out.println(String.format("SmallShield name: %s", name));
    return super.toString();
  }
}
package sk.mwk77.testfield01;

import sk.mwk77.testfield01.Shield.Shields;
import sk.mwk77.testfield01.Shield.ShieldsFactory;
import sk.mwk77.testfield01.Shield.ShieldsType;
import sk.mwk77.testfield01.Weapon.Weapons;
import sk.mwk77.testfield01.Weapon.WeaponsFactory;
import sk.mwk77.testfield01.Weapon.WeaponsType;

public class TestFactory {

  public static void main(String[] args) {

    Weapons weapon1 = WeaponsFactory.createWeapon(100,15,5,"Excalibur", WeaponsType.Axe);
    Weapons weapon2 = WeaponsFactory.createWeapon(90,12, 6, "Roxo", WeaponsType.Sword);
    Weapons weapon3 = WeaponsFactory.createWeapon(80,5, 2, "Klein", WeaponsType.Knife);

    Shields shield1 = ShieldsFactory.createShield(99, 14, 4, "Gonzo", ShieldsType.BigShield);
    Shields shield2 = ShieldsFactory.createShield(89, 11, 5, "Snicker", ShieldsType.SmallShield);
    Shields shield3 = ShieldsFactory.createShield(79, 4, 1, "Circle", ShieldsType.RoundShield);

    System.out.println("Weapon1 " + weapon1);
    System.out.println("Weapon2 " + weapon2);
    System.out.println("Weapon3 " + weapon3);

    System.out.println(weapon1.getHealth());
    weapon1.setHealth(50);
    System.out.println(weapon1.getHealth());

    System.out.println();

    System.out.println("Shield1 " + shield1);
    System.out.println("Shield2 " + shield2);
    System.out.println("Shield3 " + shield3);

    System.out.println(shield1.getHealth());
    shield1.setHealth(50);
    System.out.println(shield1.getHealth());

  }
}
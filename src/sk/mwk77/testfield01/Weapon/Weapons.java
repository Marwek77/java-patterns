package sk.mwk77.testfield01.Weapon;

public abstract class Weapons {

  public abstract String getName();

  public abstract int getWpnAtt();

  public abstract int getWpnDef();

  public abstract int getHealth();

  public abstract void setHealth(int health);

  @Override
  public String toString() {
    return String.format("is %d %d %d %s", getHealth(), getWpnAtt(), getWpnDef(), getName());
  }
}
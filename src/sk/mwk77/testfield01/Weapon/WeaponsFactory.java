package sk.mwk77.testfield01.Weapon;

public class WeaponsFactory {

  public static Weapons createWeapon(int getHealth, int getWpnAtt, int getWpnDef, String name, WeaponsType type) {

    Weapons weapons = null;

    switch (type) {
      case Axe:
        weapons = Axe.valueOf(getHealth, getWpnAtt, getWpnDef, name);
        break;
      case Sword:
        weapons = Sword.valueOf(getHealth, getWpnAtt, getWpnDef, name);
        break;
      case Knife:
        weapons = new Knife(getHealth, getWpnAtt, getWpnDef, name);
        break;
    }
    return weapons;
  }
}
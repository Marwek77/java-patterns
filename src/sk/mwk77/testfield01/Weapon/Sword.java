package sk.mwk77.testfield01.Weapon;

public class Sword extends Weapons {

  private final String name;      /* sword name */
  private final int wpnAtt;
  private final int wpnDef;

  private int health;

  static Sword valueOf(int health, int wpnAtt, int wpnDef, String name) {
    return new Sword(health, wpnAtt, wpnDef, name);
  }

  private Sword(int health, int wpnAtt, int wpnDef, String name) {
    this.health = health;
    this.wpnAtt = wpnAtt;
    this.wpnDef = wpnDef;
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public int getWpnAtt() {
    return wpnAtt;
  }

  public int getWpnDef() {
    return wpnDef;
  }

  public int getHealth() {
    return health;
  }

  public void setHealth(int health) {
    this.health = health;
  }

  @Override
  public String toString() {
    System.out.println(String.format("Sword name: %s", name));
    return super.toString();
  }
}
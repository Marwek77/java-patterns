package sk.mwk77.testfield03;

class ITCompany {

  String itcName = "naresh i technologies";

  private ITCompany() {
  }

  /* static method factory */
  static ITCompany getObjectForITCompany() {
    return new ITCompany();
  }

  /* non-static method factory */
  Employee getObjectForEmployee() {
    return new Employee();
  }
}
package sk.mwk77.testfield03;

public class FMDemo {

  public static void main(String[] args) {

    /* calling static method factory */
    ITCompany itc = ITCompany.getObjectForITCompany();
    System.out.println("company name: " + itc.itcName);

    /* calling non-static method factory */
    Employee e = itc.getObjectForEmployee();
    System.out.println("employee name: " + e.empName);
  }
}
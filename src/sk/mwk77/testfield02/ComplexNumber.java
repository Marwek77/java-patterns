package sk.mwk77.testfield02;

final class ComplexNumber {

  /**
   * Static factory method returns an object of this class.
   */
  static ComplexNumber valueOf(float real, float imaginary) {
    return new ComplexNumber(real, imaginary);
  }

  /**
   * Caller cannot see this private constructor.
   *
   * The only way to build a ComplexNumber is by calling the static
   * factory method.
   */
  private ComplexNumber(float real, float imaginary) {
    this.real = real;
    this.imaginary = imaginary;
  }

  private float real;
  private float imaginary;

  //..elided
}
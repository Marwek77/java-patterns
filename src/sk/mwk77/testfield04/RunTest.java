package sk.mwk77.testfield04;

public class RunTest {

  public static void main(String[] args) {

    Test test1 = new Test.TestBuilder()
            .withFirstName("Peter")
            .withLastName("Mayer")
            .withAge(55)
            .withPhone("0905 111 222")
            .withAddress("New York 145, USA")
            .build();

    System.out.println(test1);
  }
}
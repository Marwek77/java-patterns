package sk.mwk77.testfield04;

public class User {

  //All final attributes
  private final String firstName; // required
  private final String lastName; // required
  private final int age; // optional
  private final String phone; // optional
  private final String address; // optional

  private User(Builder builder) {
    firstName = builder.firstName;
    lastName = builder.lastName;
    age = builder.age;
    phone = builder.phone;
    address = builder.address;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public int getAge() {
    return age;
  }

  public String getPhone() {
    return phone;
  }

  public String getAddress() {
    return address;
  }

  @Override
  public String toString() {
    return "User: "+this.firstName+", "+this.lastName+", "+this.age+", "+this.phone+", "+this.address;
  }

  public static final class Builder {
    private String firstName;
    private String lastName;
    private int age;
    private String phone;
    private String address;

    public Builder(String firstName, String lastName) {
      this.firstName = firstName;
      this.lastName = lastName;
    }

    public Builder withAge(int val) {
      age = val;
      return this;
    }

    public Builder withPhone(String val) {
      phone = val;
      return this;
    }

    public Builder withAddress(String val) {
      address = val;
      return this;
    }

    //Return the finally constructed User object
    public User build() {
      User user =  new User(this);
      validateUserObject(user);
      return user;
    }

    private void validateUserObject(User user) {
      //Do some basic validations to check
      //if user object does not break any assumption of system
    }
  }
}
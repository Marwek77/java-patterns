package sk.mwk77.testfield04;

public class RunUser {

  public static void main(String[] args) {

    User user1 = new User.Builder("Lokesh", "Gupta")
            .withAge(30)
            .withPhone("123456")
            .withAddress("Fake address 1234")
            .build();

    System.out.println(user1);

    User user2 = new User.Builder("Jack", "Reacher")
            .withAge(40)
            .withPhone("5655")
            //no address
            .build();

    System.out.println(user2);

    User user3 = new User.Builder("Super", "Man")
            //No age
            //No phone
            //no address
            .build();

    System.out.println(user3);
  }
}
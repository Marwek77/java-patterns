package sk.mwk77.testfield04;

public class Game {

  //All final attributes
  private String firstName; // required
  private String lastName; // required
  private int age; // optional
  private String phone; // optional
  private String address; // optional


  private Game(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public int getAge() {
    return age;
  }

  public String getPhone() {
    return phone;
  }

  public String getAddress() {
    return address;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  @Override
  public String toString() {
    return "User: "+this.firstName+", "+this.lastName+", "+this.age+", "+this.phone+", "+this.address;
  }

  public static class GameBuilder {
    private String firstName;
    private String lastName;
    private int age;
    private String phone;
    private String address;

    GameBuilder(String firstName, String lastName) {
      this.firstName = firstName;
      this.lastName = lastName;
    }

    public static GameBuilder aGame(String firstName, String lastName) {
      return new GameBuilder(firstName, lastName);
    }

    public GameBuilder withAge(int age) {
      this.age = age;
      return this;
    }

    public GameBuilder withPhone(String phone) {
      this.phone = phone;
      return this;
    }

    public GameBuilder withAddress(String address) {
      this.address = address;
      return this;
    }

    public Game build() {
      Game game = new Game(firstName, lastName);
      game.setAge(age);
      game.setPhone(phone);
      game.setAddress(address);
      return game;
    }
  }
}
package sk.mwk77.testfield04;

public class Test {

  //All final attributes
  private String firstName; // required
  private String lastName; // required
  private int age; // optional
  private String phone; // optional
  private String address; // optional

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public int getAge() {
    return age;
  }

  public String getPhone() {
    return phone;
  }

  public String getAddress() {
    return address;
  }

  @Override
  public String toString() {
    return "User: "+this.firstName+", "+this.lastName+", "+this.age+", "+this.phone+", "+this.address;
  }

  public static final class TestBuilder {
    //All final attributes
    private String firstName; // required
    private String lastName; // required
    private int age; // optional
    private String phone; // optional
    private String address; // optional

    TestBuilder() {
    }

    public static TestBuilder aTest() {
      return new TestBuilder();
    }

    public TestBuilder withFirstName(String firstName) {
      this.firstName = firstName;
      return this;
    }

    public TestBuilder withLastName(String lastName) {
      this.lastName = lastName;
      return this;
    }

    public TestBuilder withAge(int age) {
      this.age = age;
      return this;
    }

    public TestBuilder withPhone(String phone) {
      this.phone = phone;
      return this;
    }

    public TestBuilder withAddress(String address) {
      this.address = address;
      return this;
    }

    public Test build() {
      Test test = new Test();
      test.age = this.age;
      test.address = this.address;
      test.firstName = this.firstName;
      test.phone = this.phone;
      test.lastName = this.lastName;
      return test;
    }
  }
}
package sk.mwk77.testfield04;

public class RunHome {

  public static void main(String[] args) {

    System.out.println(Home.HomeBuilder.aHome()
            .withFirstName("John")
            .withLastName("Rambo")
            .withAge(43)
            .withPhone("none")
            .withAddress("Vietnam")
            .build());
  }
}
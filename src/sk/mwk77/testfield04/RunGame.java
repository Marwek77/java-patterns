package sk.mwk77.testfield04;

public class RunGame {

  public static void main(String[] args) {

    Game game1 = new Game.GameBuilder("Alfonz", "Fly")
            .withAge(35)
            .withPhone("123")
            .withAddress("right there")
            .build();

    System.out.println(game1);

    System.out.println(Game.GameBuilder.aGame("Hero", "Anti-Hero")
            .withAge(42)
            .withPhone("456")
            .withAddress("not here")
            .build());
  }
}
package sk.mwk77.testfield04;

public class Home {

  private String firstName; // required
  private String lastName; // required
  private int age; // optional
  private String phone; // optional
  private String address; // optional


  private Home() {
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public int getAge() {
    return age;
  }

  public String getPhone() {
    return phone;
  }

  public String getAddress() {
    return address;
  }

  private void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  private void setLastName(String lastName) {
    this.lastName = lastName;
  }

  private void setAge(int age) {
    this.age = age;
  }

  private void setPhone(String phone) {
    this.phone = phone;
  }

  private void setAddress(String address) {
    this.address = address;
  }

  @Override
  public String toString() {
    return "User: "+this.firstName+", "+this.lastName+", "+this.age+", "+this.phone+", "+this.address;
  }

  public static class HomeBuilder {
    private String firstName;
    private String lastName;
    private int age;
    private String phone;
    private String address;

    private HomeBuilder() {
    }

    public static HomeBuilder aHome() {
      return new HomeBuilder();
    }

    public HomeBuilder withFirstName(String firstName) {
      this.firstName = firstName;
      return this;
    }

    public HomeBuilder withLastName(String lastName) {
      this.lastName = lastName;
      return this;
    }

    public HomeBuilder withAge(int age) {
      this.age = age;
      return this;
    }

    public HomeBuilder withPhone(String phone) {
      this.phone = phone;
      return this;
    }

    public HomeBuilder withAddress(String address) {
      this.address = address;
      return this;
    }

    public Home build() {
      Home home = new Home();
      home.setFirstName(firstName);
      home.setLastName(lastName);
      home.setAge(age);
      home.setPhone(phone);
      home.setAddress(address);
      return home;
    }
  }
}
package sk.mwk77.journaldev.abstractfactorydesignpattern.abstractfactory;

import sk.mwk77.journaldev.abstractfactorydesignpattern.beans.Computer;

public class ComputerFactory {

  public static Computer createComputer(ComputerAbstractFactory caf) {
    return caf.createComputer();
  }
}
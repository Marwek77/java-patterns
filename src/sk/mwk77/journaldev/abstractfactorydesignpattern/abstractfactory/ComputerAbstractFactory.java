package sk.mwk77.journaldev.abstractfactorydesignpattern.abstractfactory;

import sk.mwk77.journaldev.abstractfactorydesignpattern.beans.Computer;

public interface ComputerAbstractFactory {

  Computer createComputer();
}
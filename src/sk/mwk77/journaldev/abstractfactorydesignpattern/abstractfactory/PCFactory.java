package sk.mwk77.journaldev.abstractfactorydesignpattern.abstractfactory;

import sk.mwk77.journaldev.abstractfactorydesignpattern.beans.Computer;
import sk.mwk77.journaldev.abstractfactorydesignpattern.beans.PC;

public class PCFactory implements ComputerAbstractFactory {

  private String RAM;
  private String HDD;
  private String CPU;

  public PCFactory(String ram, String hdd, String cpu) {
    this.RAM = ram;
    this.HDD = hdd;
    this.CPU = cpu;
  }

  @Override
  public Computer createComputer() {
    return new PC(this.RAM, this.HDD, this.CPU);
  }
}
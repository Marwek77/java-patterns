package sk.mwk77.journaldev.abstractfactorydesignpattern.abstractfactory;

import sk.mwk77.journaldev.abstractfactorydesignpattern.beans.Computer;
import sk.mwk77.journaldev.abstractfactorydesignpattern.beans.Laptop;

public class LaptopFactory implements ComputerAbstractFactory {

  @Override
  public Computer createComputer() {
    return new Laptop();
  }
}
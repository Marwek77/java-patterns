package sk.mwk77.journaldev.abstractfactorydesignpattern.test;

import sk.mwk77.journaldev.abstractfactorydesignpattern.beans.Computer;
import sk.mwk77.journaldev.abstractfactorydesignpattern.abstractfactory.ComputerFactory;
import sk.mwk77.journaldev.abstractfactorydesignpattern.abstractfactory.LaptopFactory;
import sk.mwk77.journaldev.abstractfactorydesignpattern.abstractfactory.PCFactory;
import sk.mwk77.journaldev.abstractfactorydesignpattern.abstractfactory.ServerFactory;

public class ComputerClient {

  public static void main(String[] args) {

    Computer pc = ComputerFactory.createComputer(new PCFactory("2 GB","500 GB","2.4 GHz"));

    Computer server = ComputerFactory.createComputer(new ServerFactory("2 GB","500 GB","2.4 GHz"));

    Computer laptop = ComputerFactory.createComputer(new LaptopFactory());

    System.out.println("AbstractFactory PC Config:" + pc);
    System.out.println("AbstractFactory Server Config:" + server);
    System.out.println("AbstractFactory PC Config:" + laptop);
  }
}
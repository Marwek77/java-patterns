package sk.mwk77.journaldev.factorydesignpattern.test;

import sk.mwk77.journaldev.factorydesignpattern.beans.Computer;
import sk.mwk77.journaldev.factorydesignpattern.factory.ComputerFactory;
import sk.mwk77.journaldev.factorydesignpattern.factory.ComputerType;

public class TestFactory {

  public static void main(String[] args) {

    Computer pc = ComputerFactory.createComputer(ComputerType.PC,"2 GB","500 GB","2.4 GHz");
    Computer server = ComputerFactory.createComputer(ComputerType.SERVER,"16 GB","1 TB","2.9 GHz");

    System.out.println("Factory PC Config: " + pc);
    System.out.println("Factory Server Config: " + server);
  }
}
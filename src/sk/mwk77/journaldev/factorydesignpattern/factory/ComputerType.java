package sk.mwk77.journaldev.factorydesignpattern.factory;

public enum ComputerType {

  PC, SERVER
}
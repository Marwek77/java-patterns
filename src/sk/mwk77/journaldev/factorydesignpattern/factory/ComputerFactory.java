package sk.mwk77.journaldev.factorydesignpattern.factory;

import sk.mwk77.journaldev.factorydesignpattern.beans.Computer;
import sk.mwk77.journaldev.factorydesignpattern.beans.PC;
import sk.mwk77.journaldev.factorydesignpattern.beans.Server;

public class ComputerFactory {

  public static Computer createComputer(ComputerType type, String ram, String hdd, String cpu) {

    Computer comp = null;

    switch (type) {
      case PC:
        comp = new PC(ram, hdd, cpu);
        break;
      case SERVER:
        comp = new Server(ram, hdd, cpu);
        break;
    }
    return comp;
  }
}